# Rvbb

Un script Python pour colorer un texte d'après une image. Résultat sous la forme d'une page html.
Par Jeanne Saliou et Emma Sizun

## Système

- Linux
- OSX

## Dépendances

- Python 3
- PIL/Pillow

## Marche à suivre
- Télécharger le script python `rvbb.py`
- Dans le dossier où se trouve le script, glisser l'image de référence et le fichier texte (au format `.txt`)
- Ouvrir le terminal
- Ouvrir le dossier contenant le script : `cd chemin/du/dossier`
- Lancer le script (avec en arguments l'image de référence, puis le fichier texte) : `python3 rvbb.py nom_de_l_image.format nom_du_fichier_texte.txt` (par exemple, `python3 rvbb.py img.jpg texte.txt`)
- Un fichier `index.html` sera généré, à ouvrir dans votre navigateur !

## Paramètres
- Pour modifier le style (css) du texte généré, il suffit de changer la variable `style` dans le script

## Résultat

![Avant](bill_avant.jpg)
![Après](bill_apres.png)



