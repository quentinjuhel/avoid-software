import os, sys, math
from PIL import Image

img = sys.argv[1]
txt = sys.argv[2]

with open(txt, 'r') as t:
	texte = list(t.read())
	n_characters = len(texte) # nombre de caractères dans le texte
	
with Image.open(img) as im:
	width, height = im.width, im.height 
	x = math.sqrt( n_characters * (width/height) ) 
	y = math.sqrt( n_characters * (height/width) )
	
	im = im.resize(( math.ceil(x), math.ceil(y))) # redimensionnement de l'image selon le nombre de caractères
	
	pixel_values = list(im.getdata())

	html = ""
	
	for i in range(len(texte)):
		
		if (i>0 and i % math.ceil(x) == 0) :
			linebreak = "<br>"
		else:
			linebreak = ""
			
		html += "<span style='color:rgb" + str(pixel_values[i]) + "';>" + texte[i] + "</span>" + linebreak
		
with open("index.html", 'w') as h:
	content =  "<!DOCTYPE html><html><head><style>body{line-height:1;letter-spacing:1px;font-family:monospace;}</style></head><body>" + html + "</body></html>"
	h.write(content)
	
