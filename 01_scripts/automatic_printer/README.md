# Lancer une impression automatiquement

Ce script bash permet de lancer des impressions automatiquement depuis un dossier spécifique. Le script va vérifier toutes les x secondes si un nouveau fichier est présent dans le dossier. Si oui, il va l'imprimer et l'archiver dans un dossier donné.

## Système 
- Linux 
- OSX

## Marche à suivre
- Télécharger le fichier print-daemon.sh
- Allumer l’imprimante et connectez là à votre ordinateur
- Ouvrir le Terminal
- Lister les imprimantes connectées `lpstat -p`
- Dans la liste qui s'affiche, trouvez votre imprimante 
- Configurer le script, ouvrez le script print-daemon.sh dans un éditeur de code
- Changer le nom de l'imprimante dans la variable printer `printer=Canon_LBP7100C_7110C`
- Changer le nom du dossier d'impression et du dossier d'archive si besoin 
- Une fois la configuration terminée, lancer le script dans le Terminal: `bash print-deamon.sh` 

