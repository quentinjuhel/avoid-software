#!/bin/bash
# set -x

# settings
# lister les imprimantes connectées $ lpstat -p
# lancer le script $ bash print-deamon-no-watch.sh 

# changer le nom de l'imprimante ici
printer=Canon_LBP7100C_7110C

# changer le nom des dossiers
# archivebox étant le nom du dossier dans lequel vont être archivés les fichiers imprimés
archivebox="archivebox/"
# print étant le nom du dossier dans lequel on met les fichiers à imprimer
printinbox="printbox/"

# le script va vérifier s'il y a de nouveaux fichiers dans la printbox toutes le 10 secondes
interval=10

# create archibox and printinbox folder if they don't exist
mkdir $archivebox $printinbox

while true; do

  for step in `find $printinbox -iname "*.pdf" -type f`
  do 
  # lpr options here -> https://www.microsoft.com/resources/documentation/windows/xp/all/proddocs/en-us/lpr.mspx?mfr=true
    lpr -P $printer -o media=A4 -o fit-to-page $step
    mv -v $step $archivebox # copy in outbox (archives)
  done

  # wait
  for (( i=$interval; i>0; i--)); do
    sleep 1 &
    printf "next try in $i s \r"
    wait
    printf "                   \r"
  done
done

