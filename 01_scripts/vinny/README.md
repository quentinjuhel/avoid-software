# Vinny

Vinny est comme une vigne qui pousse sur votre image. Il sépare chaque canal de couleur, applique l'effet organique et produit un SVG avec les 3 couleurs superposées.
Par [Bonjour Monde](http://bonjourmonde.net)

## Dépendances
- Installer Python 3
- Installer la libraire python svgutils : ```pip install svgutils```
- Autotrace
- Imagemegick

## Marche à Suivre
- Télécharger le dossier vinny 
- Glisser l'image à modifier dans le dossier `/vinny`
-  Ouvrir le terminal et accéder au dossier vinny avec la commande `cd path/to/vinny`
- Utiliser la commande suivant: `./vinny mypicture.jpg`
NB: n'oubliez pas de donner l'autorisation nécessaire au script.
Ce script accepte également le traitement par lot: `./vinny *.jpg`

### Modifier les couleurs
Les couleurs sont produites avec le script python appelé `layering.py`. Pour chaque couche, vous pouvez changer la couleur du chemin en modifiant la valeur rgb sur les lignes 14, 20, 26: `child.set('style','stroke:rgb(201,47,22);fill:none;')`

### Résultat
![vinny](dalle_merged.png)


## License
[GPL](https://www.gnu.org/licenses/gpl-3.0.en.html)

