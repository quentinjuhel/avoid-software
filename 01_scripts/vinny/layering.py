import svgutils.transform as st
import xml.etree.ElementTree as ET
import os, argparse
parser = argparse.ArgumentParser()
parser.add_argument("-n", "--name", help="file name")
args = parser.parse_args()
name = args.name

#### coloring each layer
## red layer
tree = ET.parse("r.svg")
root = tree.getroot()
for child in root:
    child.set('style','stroke:#7F7F7F;fill:none;')
tree.write('r.svg')
## green layer
tree = ET.parse("g.svg")
root = tree.getroot()
for child in root:
    child.set('style','stroke:#5A5A5A;fill:none;')
tree.write('g.svg')
## blue layer
tree = ET.parse("b.svg")
root = tree.getroot()
for child in root:
    child.set('style','stroke:#222222;fill:none;')
tree.write('b.svg')

### merge the image
base = st.fromfile('r.svg')
second_svg = st.fromfile('g.svg')
third_svg = st.fromfile('b.svg')
base.append(second_svg)
base.append(third_svg)
base.save(name+'_merged.svg')
