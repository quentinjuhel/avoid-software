from asciiWriter.patterns import sinus_vertical
from asciiWriter.utils import make_lines, visit, print_lines, merge
from asciiWriter.marks import sentence, space

# Define width and height of the output
width = 70
height = 25

# As we draw multiple sinoids we will collect
# them in a list of layers
layers = []

# Loop through a range of X, X in steps of X
# (the amount of loops is the amount of layers)
for x in range(-50, 50, 5):
    # Set the pattern with the changing offset
    pattern = sinus_vertical(period=10, amplitude=40, offset=x)
    # We use a sentence to draw the text
    mark = sentence('▚▒▓▞')
    # Define a blank character
    blank = space(' ')

    # Make the canvas
    lines = make_lines(width, height)

    # Draw the sinoid, but add it to the list
    result = visit(lines, pattern, mark, blank)
    # Add it the result to the list of layers
    layers.append(result)
    


# Merge the layers into one layer again
merged = merge(width, height, blank(), layers)

# Print the result
print_lines(merged)

# Write the result in txt file
# with open("sinus.txt", 'w') as newFile:
#     for els in merged:
#         for el in els: 
#             # print(el)
#             # write each item on a new line
#             newFile.write(el) 
#     print('Done')





