# ASCII Writer 

Une bibliothèque python pour dessiner avec du texte brut dans le style ASCII (mais avec Unicode).
Par Manetta Berends

## Système 
- Linux 
- OSX

## Dépendance
- Python 3 
- Asciiwriter ```pip3 install asciiwriter```

## Marche à suivre
- Télécharger les fichiers .py
- Ouvrir le terminal et lancer le script python souhaité: `python3 line.py`
- Le résultat s'affiche dans le terminal
-Vous pouvez ensuite tester les autres exemples présents dans le dossier et modifier les variables et le texte dans les fichiers Python.