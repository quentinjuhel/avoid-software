# 2cool4cloud

Un script très artisanal qui transforme les contours d'une typo en plusieurs arcs avec [svgpathtools](https://pypi.org/project/svgpathtools/)
Par Jeanne Saliou et Emma Sizun

![2cool4cloud](2cool4cloud.jpg )

## Système

- Linux
- OSX

## Dépendances

- Python 3
- Fontforge
- Svgpathtools
- Lxml


## Marche à suivre
- Télécharger le fichier 2cool4cloud.py
- Dans le dossier où se trouve le script, glisser la typo à transformer
- Ouvrir le terminal
- Ouvrir le dossier où se trouve le script : ```cd chemin/du/dossier```
- Lancer le script : ```python3 2cool4cloud.py nom_de_la_typo.format``` (par exemple, `python3 2cool4cloud.py garamond.otf`)

## Paramètres à expérimenter dans le script 

- *coeff* : nombres d'arcs. 1 = beaucoup d'arcs, 10 = peu d'arcs. (Par défault 1)
- *stroke* : épaisseur du contour (Par défault 20)
- *rot* : rotation des arcs (Par défault 0)
- *rayon* : rayon de l'arc, sous la forme `x + yj` (Par défault 1 + 1j)

 


