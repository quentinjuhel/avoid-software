// ------------------ R A N D O M    C H A R S   S C R I P T ----------------
var charsArray = ['_', '*', '.', "+", "/", "<", ">", ";", ")", "=", '{', '}'];

var main = document.querySelector('main');
var nbOfEl = main.offsetHeight / 10;
for(var i=0; i<=nbOfEl; i++){
	var el = document.createElement("div");
	var randomChars = Math.floor(Math.random()* charsArray.length);
	el.innerHTML = charsArray[randomChars];
	el.classList.add("chars-fun");
	var randomX = Math.floor(Math.random()* window.innerWidth);
	var randomY = Math.floor(Math.random()* main.offsetHeight);
	el.style.top = randomY + "px";
	el.style.left = randomX + "px";
	main.appendChild(el);
}


// ---------- Q U E N T I N    D E M O --------

// Récupération de tous les éléments p et li de la page
var elements = document.querySelectorAll('.quentin-demo p,.quentin-demo li');

// Parcours de tous les éléments pour remplacer les espaces typographiques par une balise span
for (var i = 0; i < elements.length; i++) {
    var element = elements[i];
    var html = element.innerHTML;
    var html_modifie = html.replace(/(?!<a[^>]*>|<code[^>]*>|<img[^>]*>)( |\t)+(?![^<]*<\/a>|[^<]*<\/code>|[^<]*\/>)/g, '<span>$&</span>');
    element.innerHTML = html_modifie;
}

// Récupération de tous les éléments p et li de la page
var elements = document.querySelectorAll('.quentin-demo h1,.quentin-demo h2,.quentin-demo h3,.quentin-demo h4');

// Parcours de tous les éléments pour remplacer les espaces typographiques par une balise span
for (var i = 0; i < elements.length; i++) {
    var element = elements[i];
    var html = element.innerHTML;
    var html_modifie = html.replace(/(?!<a[^>]*>|<code[^>]*>|<img[^>]*>)([a-zA-Z])(?![^<]*<\/a>|[^<]*<\/code>|[^<]*\/>)/g, '<span>$1</span>');
    element.innerHTML = html_modifie;

    var spans = element.querySelectorAll('.quentin-demo span');

    // Parcours de toutes les balises span pour ajouter une rotation aléatoire
    for (var j = 0; j < spans.length; j++) {
        var span = spans[j];
        var deg = Math.floor(Math.random() * 45) - 10; // Rotation aléatoire entre -5 et 5 degrés
        span.style.transform = 'rotate(' + deg + 'deg)';
    }

}


		




