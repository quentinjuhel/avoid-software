# Avoid Software

*Avoid Software* est un fanzine geekos-punkos-utilos proposant des méthodes, scripts, hacks libres et open-source utiles à tout artiste, designer ou étudiant·e en école d’art et de design. Comment se passer des logiciels et utiliser des scripts pour toute action numérique? Convertir des formats de fichiers, compresser un pdf, modifier des images, renommer des fichiers… Il présente également des programmes plus complexes et expérimentaux imaginés par des designers graphiques geekos et libristes.

Accèder à la version en ligne du fanzine: ![avoidsoftware](https://avoidsoftware.sarahgarcin.com)

v0.1 (mai 2023)Avoid Software

Pour l’instant, *Avoid Software* documente les installations et les scripts pour Linux (Ubuntu / Debian) et pour macOS. Windows et autres distributions Linux à venir.

## Fonctionnement du repo
`01_scripts` contient certains programmes présentés dans le fanzine
`02_fanzine` contient le code source de la version imprimée, mis en page en HTML, CSS et Paged.js
`03_flyer` contient le code source du flyer (rigolo) de diffusion du site, mis en page en HTML, CSS et Paged.js
`04_website` contient le code source de la version web


## Crédits

Écriture et conception: Sarah Garcin, Quentin Juhel et Emma Sizun
Avec la participation de&#8239;: Manetta Berends, Bonjour Monde et Jeanne Saliou
Typographies&#8239;: <em>Baskervvol </em> de ANRT, <em>Syne</em> de Bonjour Monde et <em>Hershey</em> de Luuse</p>
Le titre <em>Avoid Software</em> est tiré du <em>Manifesto for Growth</em> de Bruce Mau


## Licence
Tous les contenus et le code source sont sous licence libre, code et code source sous licence GNU GPL, contenus sous licence Creative Commons CC-BY.