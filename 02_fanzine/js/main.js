// ---------- Q U E N T I N    D E M O --------

// Récupération de tous les éléments p et li de la page
var elements = document.querySelectorAll('.quentin-demo p,.quentin-demo li');

// Parcours de tous les éléments pour remplacer les espaces typographiques par une balise span
for (var i = 0; i < elements.length; i++) {
    var element = elements[i];
    var html = element.innerHTML;
    var html_modifie = html.replace(/(?!<a[^>]*>|<code[^>]*>|<img[^>]*>)( |\t)+(?![^<]*<\/a>|[^<]*<\/code>|[^<]*\/>)/g, '<span>$&</span>');
    element.innerHTML = html_modifie;
}

// Récupération de tous les éléments p et li de la page
var elements = document.querySelectorAll('.quentin-demo h1,.quentin-demo h2,.quentin-demo h3,.quentin-demo h4');

// Parcours de tous les éléments pour remplacer les espaces typographiques par une balise span
for (var i = 0; i < elements.length; i++) {
    var element = elements[i];
    var html = element.innerHTML;
    var html_modifie = html.replace(/(?!<a[^>]*>|<code[^>]*>|<img[^>]*>)([a-zA-Z])(?![^<]*<\/a>|[^<]*<\/code>|[^<]*\/>)/g, '<span>$1</span>');
    element.innerHTML = html_modifie;

    var spans = element.querySelectorAll('.quentin-demo span');

    // Parcours de toutes les balises span pour ajouter une rotation aléatoire
    for (var j = 0; j < spans.length; j++) {
        var span = spans[j];
        var deg = Math.floor(Math.random() * 45) - 10; // Rotation aléatoire entre -5 et 5 degrés
        span.style.transform = 'rotate(' + deg + 'deg)';
    }

}


// ------------------ R A N D O M    C H A R S   S C R I P T ----------------
var charsArray = ['_', '*', '.', "+", "/", "<", ">", ";", ")", "=", '{', '}'];


class addChars extends Paged.Handler {
  constructor(chunker, polisher, caller) {
    super(chunker, polisher, caller);
  }
  afterRendered(pages){
  	pages.forEach(page => {
  		// console.log(page);
  		if(page.element.classList.contains('pagedjs_introduction_page') || page.element.classList.contains('pagedjs_cover_page') || page.element.classList.contains('pagedjs_toc_page')){
				var nbOfEl = 200;
				for(var i=0; i<=nbOfEl; i++){
					var el = document.createElement("div");
					var randomChars = Math.floor(Math.random()* charsArray.length);
					el.innerHTML = charsArray[randomChars];
					el.classList.add("chars-fun");
					var randomX = Math.floor(Math.random()*(630 - 25) - 50);
					var randomY = Math.floor(Math.random()*(780 - 25) + 25);
					el.style.top = randomY + "px";
					el.style.left = randomX + "px";
					page.element.appendChild(el);

				}
			}
  	});
  }
}   



Paged.registerHandlers(addChars);


class glossaire extends Paged.Handler {
    constructor(chunker, polisher, caller) {
      super(chunker, polisher, caller);
    }

    beforeParsed(content){          
        let glossRef = content.querySelectorAll('.gloss-ref');
        // let slugGlossRef = string_to_slug(glossRef);
        let glossEl = content.querySelectorAll('.def-wrapper > h2');
        // let slugGlossEl = string_to_slug(slugGlossRef);
        // console.log(glossRef, glossEl);
        for(var i= 0; i < glossRef.length; i++){
            let slugGlossRef = string_to_slug(glossRef[i].innerHTML);
            // console.log(slugGlossRef);
            
        }
        

    }
    
  }

Paged.registerHandlers(glossaire);



class toc extends Paged.Handler {
    constructor(chunker, polisher, caller) {
      super(chunker, polisher, caller);
    }

    beforeParsed(content){          
      createToc({
        content: content,
        tocElement: '#table-of-contents',
        titleElements: [ '.toc-el' ]
      });
    }
    
  }
Paged.registerHandlers(toc);




function createToc(config){
    const content = config.content;
    const tocElement = config.tocElement;
    const titleElements = config.titleElements;
    
    let tocElementDiv = content.querySelector(tocElement);
    let tocUl = document.createElement("ul");
    tocUl.id = "list-toc-generated";
    tocElementDiv.appendChild(tocUl); 

    // add class to all title elements
    let tocElementNbr = 0;
    for(var i= 0; i < titleElements.length; i++){
        
        let titleHierarchy = i + 1;
        let titleElement = content.querySelectorAll(titleElements[i]);  


        titleElement.forEach(function(element) {

            // add classes to the element
            element.classList.add("title-element");
            element.setAttribute("data-title-level", titleHierarchy);

            // add id if doesn't exist
            tocElementNbr++;
            idElement = element.id;
            if(idElement == ''){
                element.id = 'title-element-' + tocElementNbr;
            } 
            let newIdElement = element.id;

        });

    }

    // create toc list
    let tocElements = content.querySelectorAll(".title-element");  

    for(var i= 0; i < tocElements.length; i++){
        let tocElement = tocElements[i];

        let tocNewLi = document.createElement("li");

        // Add class for the hierarcy of toc
        tocNewLi.classList.add("toc-element");
        tocNewLi.classList.add("toc-element-level-" + tocElement.dataset.titleLevel);

        // Keep class of title elements
        let classTocElement = tocElement.classList;
        for(var n= 0; n < classTocElement.length; n++){
            if(classTocElement[n] != "title-element"){
                tocNewLi.classList.add(classTocElement[n]);
            }   
        }

        // Create the element
        tocNewLi.innerHTML = '<a href="#' + tocElement.id + '">' + tocElement.innerHTML + '</a>';
        tocUl.appendChild(tocNewLi);  
    }

}

function string_to_slug (str) {
    str = str.replace(/^\s+|\s+$/g, ''); // trim
    str = str.toLowerCase();
  
    // remove accents, swap ñ for n, etc
    var from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;";
    var to   = "aaaaeeeeiiiioooouuuunc------";
    for (var i=0, l=from.length ; i<l ; i++) {
        str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
    }

    str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
        .replace(/\s+/g, '-') // collapse whitespace and replace by -
        .replace(/-+/g, '-'); // collapse dashes

    return str;
}

